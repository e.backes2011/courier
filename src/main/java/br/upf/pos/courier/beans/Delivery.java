package br.upf.pos.courier.beans;

import java.io.Serializable;
import java.lang.Long;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: Delivery
 *
 */
@Entity
@Table(name="delivery")
public class Delivery implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_delivery")
	@SequenceGenerator(name="seq_delivery", sequenceName="seq_delivery", allocationSize=1)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@NotNull(message="Status may not be empty.")
	private DeliveryStatusEnum status;
	
	@OneToMany(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="delivery", fetch=FetchType.EAGER)
	@NotNull(message="A delivery may have items.")
	@Size(min=1, message="A delivery may have at least {min} item(s).")
	private List<DeliveryItem> items;
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@NotNull(message="Pickup address may not be empty.")
	private PickupAddress pickup;
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true, mappedBy="delivery")
	@NotNull(message="Dropoff address may not be empty.")
	private DropoffAddress dropoff;
	
	private static final long serialVersionUID = 1L;

	public Delivery() {
		super();
	}   
	public Delivery(Long id, DeliveryStatusEnum status) {
		super();
		this.id = id;
		this.status = status;
	}
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public DeliveryStatusEnum getStatus() {
		return status;
	}
	public void setStatus(DeliveryStatusEnum status) {
		this.status = status;
	}
	public List<DeliveryItem> getItems() {
		return items;
	}
	public void setItems(List<DeliveryItem> items) {
		this.items = items;
	}
	@Override
	public String toString() {
		return "Delivery [id=" + id + ", status=" + status + ", items=" + items + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Delivery other = (Delivery) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public PickupAddress getPickup() {
		return pickup;
	}
	public void setPickup(PickupAddress pickup) {
		this.pickup = pickup;
	}
	public DropoffAddress getDropoff() {
		return dropoff;
	}
	public void setDropoff(DropoffAddress dropoff) {
		this.dropoff = dropoff;
	}
   
}
