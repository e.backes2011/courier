package br.upf.pos.courier.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: DeliveryItem
 *
 */
@Entity
@Table(name="delivery_item")
public class DeliveryItem implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_delivery_item")
	@SequenceGenerator(name="seq_delivery_item", sequenceName="seq_delivery_item", allocationSize=1)
	private Long id;
	
	@Column
	@Length(max=200, message="Obs may not be bigger than {max} characters.")
	private String obs;
	
	@Column
	@NotBlank(message="Description may not be empty.")
	@Length(min=3, max=50)
	private String description;
	
	@Column
	@NotNull(message="Quantity may not be null.")
	@Min(value=0, message="Quantity may not be less then {value}.")
	private Integer quantity;
	
	@ManyToOne(optional=false)
	@NotNull(message="Delivery may not be null.")
	private Delivery delivery;
	
	private static final long serialVersionUID = 1L;

	public DeliveryItem() {
		super();
	}   
	public DeliveryItem(Long id, String obs, String description, Integer quantity) {
		super();
		this.id = id;
		this.obs = obs;
		this.description = description;
		this.quantity = quantity;
	}
	public DeliveryItem(Long id, String obs, String description, Integer quantity, Delivery delivery) {
		super();
		this.id = id;
		this.obs = obs;
		this.description = description;
		this.quantity = quantity;
		this.delivery = delivery;
	}
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getObs() {
		return this.obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}   
	public Delivery getDelivery() {
		return this.delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
	@Override
	public String toString() {
		return "DeliveryItem [id=" + id + ", obs=" + obs + ", description=" + description + ", quantity=" + quantity
				+ "]";
	}
   
}
