package br.upf.pos.courier.beans;

import br.upf.pos.courier.beans.Person;
import java.io.Serializable;
import java.lang.String;
import java.security.NoSuchAlgorithmException;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Client
 *
 */
@Entity
@PrimaryKeyJoinColumn(name="client_id", referencedColumnName="id")
@Table(name="client")
public class Client extends Person implements Serializable {

	@Column
	@NotBlank(message="Cpf/Cnpj may not be empty.")
	private String cpfCnpj;
	private static final long serialVersionUID = 1L;

	public Client() {
		super();
	}   
	public Client(Long id, String name, String email, String password, String phone, String cpfCnpj) throws NoSuchAlgorithmException {
		super(id, name, email, password, phone);
		this.cpfCnpj = cpfCnpj;
	}
	public String getCpfCnpj() {
		return this.cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	@Override
	public String toString() {
		return "Client [cpfCnpj=" + cpfCnpj + ", getId()=" + getId() + ", getName()=" + getName() + ", getEmail()="
				+ getEmail() + ", getPassword()=" + getPassword() + ", getPhone()=" + getPhone() + "]";
	}
   
}
