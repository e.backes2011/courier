package br.upf.pos.courier.beans;

import br.upf.pos.courier.beans.Person;
import java.io.Serializable;
import java.lang.String;
import java.security.NoSuchAlgorithmException;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Courier
 *
 */
@Entity
@PrimaryKeyJoinColumn(name="courier_id", referencedColumnName="id")
@Table(name="courier")
public class Courier extends Person implements Serializable {

	@Column
	@NotBlank(message="Cpf may not be empty.")
	private String cpf;
	private static final long serialVersionUID = 1L;

	public Courier() {
		super();
	}   
	public Courier(Long id, String name, String email, String password, String phone, String cpf) throws NoSuchAlgorithmException {
		super(id, name, email, password, phone);
		this.cpf = cpf;
	}
	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	@Override
	public String toString() {
		return "Courier [cpf=" + cpf + ", getId()=" + getId() + ", getName()=" + getName() + ", getEmail()="
				+ getEmail() + ", getPassword()=" + getPassword() + ", getPhone()=" + getPhone() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + "]";
	}
   
}
