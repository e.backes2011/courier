package br.upf.pos.courier.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Address
 *
 */
@Entity
@Table(name="dropoff_address")
public class DropoffAddress implements Serializable {
	
	@Id
	@GeneratedValue(generator="generator")
	@GenericGenerator(name="generator", strategy="foreign", parameters=@Parameter(name="property", value="delivery"))
	private Long deliveryId;
	
	@OneToOne(fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	@NotNull(message="Delivery may not be null.")
	private Delivery delivery;

	@Column
	@NotBlank(message="Street may not be empty.")
	@Length(min=3, max=150)
	private String street;
	
	@Column
	@NotNull(message="Number may not be empty.")
	private Integer number;
	
	@Column
	@Length(max=150, message="Complement may not be bigger than {max} characters.")
	private String complement;
	
	@Column
	@NotBlank(message="Name may not be empty.")
	@Length(min=3, max=50, message="Name may be between {min} and {max} characters.")
	private String name;
	
	@Column
	@Length(max=15, message="Phone may not be bigger than {max} characters.")
	private String phone;
	
	@Column
	private Double lat;
	
	@Column
	private Double lng;
	private static final long serialVersionUID = 1L;

	public DropoffAddress() {
		super();
	}   
	public DropoffAddress(String street, Integer number, String complement, String name, String phone, Double lat,
			Double lng) {
		super();
		this.street = street;
		this.number = number;
		this.complement = complement;
		this.name = name;
		this.phone = phone;
		this.lat = lat;
		this.lng = lng;
	}
	public Long getDeliveryId() {
		return this.deliveryId;
	}

	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}   
	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}   
	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}   
	public String getComplement() {
		return this.complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}   
	public Double getLat() {
		return this.lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}   
	public Double getLng() {
		return this.lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}
	@Override
	public String toString() {
		return "Address [delivery=" + deliveryId + ", street=" + street + ", number=" + number + ", complement=" + complement
				+ ", name=" + name + ", phone=" + phone + ", lat=" + lat + ", lng=" + lng + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deliveryId == null) ? 0 : deliveryId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DropoffAddress other = (DropoffAddress) obj;
		if (deliveryId == null) {
			if (other.deliveryId != null)
				return false;
		} else if (!deliveryId.equals(other.deliveryId))
			return false;
		return true;
	}
	public Delivery getDelivery() {
		return delivery;
	}
	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
   
}
