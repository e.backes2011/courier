package br.upf.pos.courier.beans;

public enum DeliveryStatusEnum {
	
	open, pickup, dropoff, finished

}
