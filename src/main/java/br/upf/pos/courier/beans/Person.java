package br.upf.pos.courier.beans;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity implementation class for Entity: Person
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name="person")
public abstract class Person implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_person")
	@SequenceGenerator(name="seq_person", sequenceName="seq_person", allocationSize=1)
	private Long id;
	
	@Column
	@NotBlank(message="Name may not be empty.")
	@Length(min=2, max=50)
	private String name;
	
	@Column
	@NotBlank(message="Email may not be empty.")
	@Email(message="Must be a valid email.")
	private String email;
	
	@Column
	@NotBlank(message="Password may not be empty.")
	private String password;
	
	@Column
	private String phone;
	private static final long serialVersionUID = 1L;

	public Person() {
		super();
	}   
	public Person(Long id, String name, String email, String password, String phone) throws NoSuchAlgorithmException {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = Person.hashPassword(password).toString();
		this.phone = phone;
	}
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) throws NoSuchAlgorithmException {
		this.password = Person.hashPassword(password).toString();
	}   
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", phone="
				+ phone + "]";
	}
	
	public static byte[] hashPassword(String password) throws NoSuchAlgorithmException {
		return MessageDigest.getInstance("MD5").digest(password.getBytes());
	}
   
}
