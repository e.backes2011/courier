package br.upf.pos.courier.dao;

import java.util.List;

import javax.persistence.EntityManager;

public class GenericDaoImpl implements GenericDao {
	
	private EntityManager em;
	
	public GenericDaoImpl(EntityManager em) {
		super();
		this.em = em;
	}

	@Override
	public <T> T persist(T obj) throws Exception {
		em.getTransaction().begin();
		obj = em.merge(obj);
		em.getTransaction().commit();
		
		return obj;
	}

	@Override
	public <T> void remove(Class<T> objClass, Long id) throws Exception {
		em.getTransaction().begin();
		em.remove(em.find(objClass, id));
		em.getTransaction().commit();
	}

	@Override
	public <T> T getById(Class<T> objClass, Long id) {
		T t = em.find(objClass, id);
		
		return t;
	}

	@Override
	public <T> List<T> get(Class<T> objClass) {
		List<T> list = em.createQuery("from "+objClass.getSimpleName()+" o").getResultList();
		
		return list;
	}

	@Override
	public <T> List<T> get(Class<T> objClass, String order) {
//		em = JPAUtil.getEntityManager();
		
		List<T> list = em.createQuery("from "+objClass.getSimpleName()+" o order by o."+order).getResultList();
		
//		em.close();
		
		return list;
	}

	@Override
	public <T> List<T> get(Class<T> objClass, String order, int maxResults, int firstResult) {
//		em = JPAUtil.getEntityManager();
		
		List<T> list = em.createQuery("from "+objClass.getSimpleName()+" o order by o."+order)
				.setFirstResult(firstResult)
				.setMaxResults(maxResults)
				.getResultList();
		
//		em.close();
		
		return list;
	}

}
