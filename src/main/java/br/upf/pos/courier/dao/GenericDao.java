package br.upf.pos.courier.dao;

import java.util.List;

public interface GenericDao {
	
    public <T> T persist(T obj) throws Exception;

    public <T> void remove(Class<T> objClass, Long id) throws Exception;

    public <T> T getById(Class<T> objClass, Long id);

    public <T> List<T> get(Class<T> objClass);

    public <T> List<T> get(Class<T> objClass, String order);

    public <T> List<T> get(Class<T> objClass, String order, int maxResults, int firstResult);

}
