package br.upf.pos.courier.beans;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DropoffAddressTest {
	
	private static Validator validator;
	
	@BeforeClass
	public static void init() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}
	
	@Test
	public void shouldFailValidateAttributes() {
		DropoffAddress address = new DropoffAddress();
		Set<ConstraintViolation<DropoffAddress>> violations = validator.validate(address);
		Assert.assertEquals(4, violations.size());
	}
	
	@Test
	public void shouldValidateAttributes() {
		DropoffAddress address = new DropoffAddress("test street", 123, null, "test name", null, null, null);
		Delivery delivery = new Delivery(null, DeliveryStatusEnum.open);
		address.setDelivery(delivery);
		Set<ConstraintViolation<DropoffAddress>> violations = validator.validate(address);
		Assert.assertEquals(0, violations.size());
	}

}
