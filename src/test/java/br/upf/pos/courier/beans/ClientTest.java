package br.upf.pos.courier.beans;

import java.security.NoSuchAlgorithmException;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ClientTest {

	private static Validator validator;
	
	@BeforeClass
	public static void init() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}
	
	@Test
	public void shouldFailValidateAttributes() {
		Client c = new Client();
		Set<ConstraintViolation<Client>> violations = validator.validate(c);
		Assert.assertEquals(4, violations.size());
	}
	
	@Test
	public void shouldValidateEmailAttribute() throws NoSuchAlgorithmException {
		Client c = new Client(null, "test name", "asd", "1234", null, "000.000.000-00");
		Set<ConstraintViolation<Client>> violations = validator.validate(c);
		Assert.assertEquals(1, violations.size());
		Assert.assertEquals("Must be a valid email.", violations.iterator().next().getMessage());
	}
	
	@Test
	public void shouldValidateAttributes() throws NoSuchAlgorithmException {
		Client c = new Client(null, "test name", "test@test.com", "1234", null, "000.000.000-00");
		Set<ConstraintViolation<Client>> violations = validator.validate(c);
		Assert.assertEquals(0, violations.size());
	}
	
	@Test
	public void shouldHashAString() throws NoSuchAlgorithmException {
		String password = "1234";
		String hashedPassword = Person.hashPassword(password).toString();
		Assert.assertEquals(true, (password.length() != hashedPassword.length()));
	}
	
}
