package br.upf.pos.courier.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeliveryTest {
	
	private static Validator validator;
	
	@BeforeClass
	public static void init() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}
	
	@Test
	public void shouldFailValidateAttributes() {
		Delivery d = new Delivery();
		Set<ConstraintViolation<Delivery>> violations = validator.validate(d);
		Assert.assertEquals(4, violations.size());
	}
	
	@Test
	public void shouldValidateAttributes() {
		Delivery delivery = new Delivery();
		delivery.setStatus(DeliveryStatusEnum.open);
		PickupAddress pickup = new PickupAddress("test road", 1234, null, "test name", null, null, null);
		pickup.setDelivery(delivery);
		delivery.setPickup(pickup);
		DropoffAddress dropoff = new DropoffAddress("test road", 123, null, "test name", null, null, null);
		dropoff.setDelivery(delivery);
		delivery.setDropoff(dropoff);
		List<DeliveryItem> items = new ArrayList<>();
		DeliveryItem item = new DeliveryItem(null, null, "test description", 1);
		items.add(item);
		delivery.setItems(items);
		
		Set<ConstraintViolation<Delivery>> violations = validator.validate(delivery);
		Assert.assertEquals(0, violations.size());
	}

}
