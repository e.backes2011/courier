package br.upf.pos.courier.beans;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeliveryItemTest {
	
	private static Validator validator;
	
	@BeforeClass
	public static void init() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}
	
	@Test
	public void shouldFailValidateAttributes() {
		DeliveryItem item = new DeliveryItem();
		Set<ConstraintViolation<DeliveryItem>> violations = validator.validate(item);
		Assert.assertEquals(3, violations.size());
	}
	
	@Test
	public void shouldValidateAttributes() {
		DeliveryItem item = new DeliveryItem(null, null, "test description", 1);
		Delivery delivery = new Delivery(null, DeliveryStatusEnum.open);
		item.setDelivery(delivery);
		Set<ConstraintViolation<DeliveryItem>> violations = validator.validate(item);
		Assert.assertEquals(0, violations.size());
	}

}
