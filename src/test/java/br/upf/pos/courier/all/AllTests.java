package br.upf.pos.courier.all;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.upf.pos.courier.beans.ClientTest;
import br.upf.pos.courier.beans.CourierTest;
import br.upf.pos.courier.beans.DeliveryItemTest;
import br.upf.pos.courier.beans.DeliveryTest;
import br.upf.pos.courier.beans.DropoffAddressTest;
import br.upf.pos.courier.beans.PickupAddressTest;
import br.upf.pos.courier.dao.GenericDaoImplTest;

@RunWith(Suite.class)
@SuiteClasses({
	ClientTest.class,
	CourierTest.class,
	DeliveryItemTest.class,
	DeliveryTest.class,
	DropoffAddressTest.class,
	PickupAddressTest.class,
	GenericDaoImplTest.class
})
public class AllTests { }
