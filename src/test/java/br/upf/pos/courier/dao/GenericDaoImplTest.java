package br.upf.pos.courier.dao;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.upf.pos.courier.beans.Client;

public class GenericDaoImplTest {
	
	private GenericDao mockDao;
	private Client client;
	
	@Before
	public void init() throws Exception {
		mockDao = mock(GenericDaoImpl.class);
		client = mock(Client.class);
		
		when(mockDao.persist(any(Client.class))).thenReturn(client);
		when(mockDao.getById(Client.class, 1L)).thenReturn(client);
	}
	
	@Test
	public void shouldPersist() throws Exception {
		mockDao.persist(client);
		verify(mockDao, times(1)).persist(anyObject());
	}
	
	@Test
	public void shouldNotPersist() {
		try {
			mockDao.persist(new Client(null, "", "asd@asd.com", "123321", null, "000.000.000-00"));
		} catch (Exception e) {
			Assert.assertEquals(true, true);
		}
	}
	
	@Test
	public void shouldDelete() throws Exception {
		mockDao.remove(Client.class, client.getId());
		verify(mockDao, times(1)).remove(Client.class, client.getId());
	}
	
	@Test
	public void shouldGetById() {
		mockDao.getById(Client.class, 1L);
		verify(mockDao, times(1)).getById(Client.class, 1L);
	}
}